import { el, mount, setChildren } from 'redom';

import 'normalize.css';
import './style.sass';

import HeaderElement from './modules/header/header.js';
import FooterElement from './modules/footer/footer.js';
import PayformElement from './modules/forms/payform.js';
import HeroElement from './modules/hero/hero';
import TemplateSectionElement from './modules/sections/TemplateSectionElement';
import CONTENT from './modules/content';

const main = el('main', { class: 'main' });
const HEADER = new HeaderElement();
const FOOTER = new FooterElement();
const hero = new HeroElement();
const container = el('.container');
const about = new TemplateSectionElement(CONTENT.about.name, CONTENT.about.heading, CONTENT.about.content);
const form = new PayformElement('pay', '1');
const form2 = new PayformElement('Donate2', '2');

mount(main, container);

setChildren(window.document.body, [
  HEADER.element,
  main,
  FOOTER.element,
]);

form.assembly(about.element);
// form2.assembly(about.element, true);
hero.assembly(main);
about.assembly(main);
