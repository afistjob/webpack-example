import TextElement from './contentElements/TextElement'

const aboutTxt = 'Эта блочная библиотека, построеная на css grid. Окончательная гибель старечка IE развязала руки для разработки библиотеки, построенной сключительно на гридах. Основная задача этой библиотеки быть минималистичной, понятной и простой в использовании. Так же в принцип библиотеки - доставлять разработчикам радость от конструкторского потанцеала и перфекционизма. Но опыта разработчика этой библиотеки пока мало, хотя амбиций хоть отбавляй, раз он решился сделать такое. Впрочем, пока она остаётся исключительно для личного пользования. О том, что уже реализованно в ней (а именно выполненное задание), вы можете узнать вот здесь ->'

const CONTENT = {
  about: {
    name: 'about',
    heading: 'О библиотеке',
    content: aboutTxt
  }
}

export default CONTENT
