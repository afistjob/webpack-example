import { el, mount } from 'redom'
import imgHero from '../../assets/img/brand/imgHero.png'
import './hero.sass'


export default class HeroElement {
  constructor(title, description, img) {
    this.title = el('h1', {class: 'hero__heading'}, title || 'G-FRAME')
    this.description = el('p', {class: 'hero__desc'}, description || 'G-FRAME - это персональная библиотека для будущих разработок. Она создана после прохождения курса на SkillBox и, вероятно, я буду просто использовать её в будушем.')
    this.img = el('img', {class: 'hero__box-img', alt: "g-frame", src: img || imgHero})
    this._titleBox = el('.hero__box', [this.title, this.img])
    this.element = el('section.container.hero', [ this._titleBox, this.description ])

    this._imgOff = () => {
      this.img.remove()
      this._titleBox.classList.add('hero__box-inverse')
    }
  }
  assembly(container = document.body) {
    this._titleBox.addEventListener('click', () => { this._imgOff()} )
    mount(container, this.element)
  }
}
