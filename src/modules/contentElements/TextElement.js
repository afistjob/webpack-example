import { el } from 'redom'

export default class TextElement {
  constructor(content) {
    this.content = el(`p.text`, content)
  }
}
