import { el, setChildren, mount } from 'redom'

export default class TemplateSectionElement {
  constructor(name, heading, content) {
    this.content = el(`p.section__txt.${name}__txt`, content)
    this.heading = el(`h2.section__heading.${name}__heading`, heading)
    this.element = el(`section.container.section.${name}`,[ this.heading, this.content ])
  }
  assembly(container = document.body) {
    mount(container, this.element)
  }
}
