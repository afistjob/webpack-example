import { el, mount, setChildren } from 'redom';
import './forms.sass';

import { SVG, BRAND } from '../collector';

const payment = require('payment');
const emailValid = require('email-validator');

export default class PayformElement {
  constructor(btnName = 'pay', id = 'id', formName = '${Заголовок для формы оплаты}') {
    this.btnOpen = el(`button.btn.payform__open.payform__open-${id}`, btnName || 'оплата', { 'data-form': `${id}-open` });
    this.btnSuccess = el('button.btn.payform__btn.success', btnName, { type: 'submit', disabled: true }); // <input type="submit" value="Send Request">
    this.form = el('form.payform.bg-dark', { 'data-form': `${id}-form`, id: `form-${id}, autofocus` });
    this.description = el('h3.payform__description', `${formName}`);
    this.card = el('.payform__card');
    this.date = el('.payform__date');
    this.CVV = el('.payform__cvv');
    this.mail = el('.payform__mail');
    this.headingCardType = el('h4.payform__card__type', 'Тип карты');
    this.headingCard = el('h4.payform__heading.number', 'NUMBER');
    this.headingDate = el('h4.payform__heading', 'DATE');
    this.headingCVV = el('h4.payform__heading', 'CVC/CVV');
    this.headingMail = el('h4.payform__heading', 'E-MAIL');
    this.inputCard = el('input.input.payform__input', {
      placeholder: '0000 0000 0000 0000', name: 'number', id: 'number', autocomplite: 'section-payment billing cc-number',
    });
    this.inputDate = el('input.input.payform__input', {
      placeholder: '00/00', name: 'date', id: 'date', autocomplite: 'section-payment billing cc-exp',
    });
    this.inputCVV = el('input.input.payform__input', {
      placeholder: '000', name: 'cvv', id: 'cvv', autocomplite: 'section-payment billing',
    });
    this.inputMail = el('input.input.payform__input', {
      type: 'email', placeholder: '@email', name: 'email', id: 'email', autocomplite: 'section-payment billing email',
    });
    this.svgIcon = 'generic';
    this._id = id;
    this._modal = el('modal.modal.modal-open', { 'data-form': `${id}-modal` });
    this._modalBg = el('.modal__bg');
    this._modalBack = el('button.btn.modal-back', 'назад', { type: 'button' });
    this._cardType = { type: 'generic' };
    this._cardTypeIcon = el('img.payform__card__type', { src: SVG[this._cardType.type] });
    this._data = {};

    this._modalOpen = () => {
      this.btnOpen.addEventListener('click', () => {
        if (!document.getElementById(`form-${id}`)) {
          mount(document.body, this._modal);
        } else {
          this._modal.classList.add('modal-open');
        }

        [this._modalBack, this._modalBg].forEach((btn) => {
          btn.addEventListener('click', () => {
            this._modal.classList.remove('modal-open');
          });
        });
      });
    };

    this._validationOn = () => {
      const formInputs = [this.inputCard, this.inputDate, this.inputCVV, this.inputMail];

      function validateWatcher(input) {
        if (
          input.id === 'number' && !payment.fns.validateCardNumber(input.value)
          || input.id === 'date' && !payment.fns.validateCardExpiry(input.value)
          || input.id === 'email' && !emailValid.validate(input.value)
          || input.id === 'cvv' && !payment.fns.validateCardCVC(input.value) && input.value.length || !input.value.trim()
        ) { return true; } return false;
      }

      function setInvalid(element, value) {
        element.classList.add(value);
        element.previousSibling.classList.add(`${value}-txt`);
        element.addEventListener('focus', () => {
          element.classList.remove(value);
          element.previousSibling.classList.remove(`${value}-txt`);
        });
      }

      payment.formatCardNumber(this.inputCard);
      payment.formatCardExpiry(this.inputDate);
      payment.formatCardCVC(this.inputCVV);

      formInputs.forEach((input) => {
        input.addEventListener('input', () => {
          if (input.value.trim()) {
            if (validateWatcher(input)) {
              input.classList.remove('valid');
              this._data[input.name] = 'invalid';
              input.addEventListener('blur', () => { if (!input.classList.contains('valid') && validateWatcher(input)) { setInvalid(input, 'invalid'); } });
            } else if (!input.classList.contains('valid')) {
              setInvalid(input, 'valid');
              input.classList.remove('invalid');
              this._data[input.name] = input.value;
              if (Object.keys(this._data).length === 4) { this.btnSuccess.removeAttribute('disabled'); }
            }
          }
        });
      });

      this.inputCard.addEventListener('input', () => {
        if (payment.fns.cardType(this.inputCard.value)) {
          this._cardType = payment.getCardArray().find((card) => card.type === payment.fns.cardType(this.inputCard.value));
          this._cardTypeIcon.src = SVG[this._cardType.type];
          this.form.classList.add(this._cardType.type);
        } else {
          this.form.classList.remove(this._cardType.type);
          this._cardType = { type: 'generic' };
          this._cardTypeIcon.src = SVG[this._cardType.type];
        }

        this.svgCard = this._cardType.type;
        this.inputCVV.maxLength = this._cardType.cvcLength[0] || 3;
      });

      this.form.addEventListener('submit', (e) => {
        e.preventDefault();
        for (let i = 0; i < this.form.elements.length; i++) { const input = this.form.elements[i]; }
        console.log(this._data);
      });
    };
  }

  assembly(container, modal = false) {
    mount(this.card, this._cardTypeIcon);

    mount(this.card, this.headingCard);
    mount(this.card, this.inputCard);
    mount(this.date, this.headingDate);
    mount(this.date, this.inputDate);
    mount(this.CVV, this.headingCVV);
    mount(this.CVV, this.inputCVV);
    mount(this.mail, this.headingMail);
    mount(this.mail, this.inputMail);

    setChildren(this.form, [this.description, this.card, this.date, this.CVV, this.mail, this.btnSuccess]);

    if (modal) {
      mount(container, this.btnOpen);
      mount(this._modal, this.form);
      mount(this._modal, this._modalBg);
      this.form.classList.add('payform__modal', 'modal__box');
      this._modalOpen();
    } else {
      mount(container, this.form);
    }

    this._validationOn();
  }
}
