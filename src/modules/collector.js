export const SVG = {};
export const BRAND = {};

function importIMG(r, array) {
  r.keys().forEach((key) => {
    array[key.split(/\./)[1].replace(/\//, '')] = r(key)
  });
}

importIMG(require.context('../assets/img/svg', true, /\.svg$/), SVG)
importIMG(require.context('../assets/img/brand', true, /\.png$/), BRAND)
importIMG(require.context('../assets/img/brand', true, /\.jpg$/), BRAND)
