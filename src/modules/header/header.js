import { el, mount } from 'redom'
import logo from './logo.png'
import './header.sass'

export default class HeaderElement {
  constructor(logoLink, text) {
    this.logo =         el('img.header__logo', { src: logoLink = logo})
    this.text =         el('p.header__txt', text || 'Просто используй это')
    this._container =   el('.container', [ this.logo, this.text ])
    this.element =      el('header.header.bg-dark', this._container)
  }
  assembly(container = document.body) {
    mount(container, this._container)
  }
}
