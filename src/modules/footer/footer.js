import { el, mount } from 'redom'
import logo from './logo.png'
import './footer.sass'

export default class FooterElement {
  constructor(logoLink, text) {
    this.logo =         el('img.footer__logo', { src: logoLink = logo})
    this.text =         el('p.footer__txt', text || 'Просто используй это')
    this._container =   el('.container', [ this.logo, this.text ])
    this.element =      el('footer.footer.bg-dark', this._container)
  }
  assembly(container = document.body) {
    mount(container, this._container)
  }
}
