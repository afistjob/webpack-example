
*********************************************************************

Это пример рабочего кода для FE разработки на базе сборщика Webpack

*********************************************************************






*********************************************************************


Подготовка и запуск

- npm i - для подгрузки всех необходимых модулей

- npm run dev - старт webpack в режиме разработки.
В консоле появиятся адреса локального сервера.

- npm run build - старт продакшн-сборщика webpack.
результат появится в папке dist.



*********************************************************************






*********************************************************************


Что в репе:

- webpack.config.js - инициализация и настройка webpack

- index.html - html который можно использовать как шаблон.
В данном примере он не используется. Вместо него есть
html-webpack-plugin

- В корне также могут располагаться конфиги для
git, eslint, editorconfig, prettier, jest и прочее.

- src - фолдер разработки.

- src/style.sass, src/index.js - точки входа js и sass.

- src/styles - фолдер стилей.

- src/modules - фолдер модулей разработки.
В этом примере html собирается из js.
У каждого отдельного модуля так же есть свой sass.

- src/assets - фолдер исходников изображений, шрифтов и прочего.

- dist - фолдер содержащий в себе релизную версию сайта.
